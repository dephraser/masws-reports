% Multiagent Semantic Web Systems -- Data Conversion Report
% Kit Barnes, Oliver Kingshott, David Fraser, James Hulme

# Conversion to RDF

As suggested in the previous report, Python was used to produce Turtle-format
RDF from the input CSV data. 

After removing any commentary, notes and non-data (as is typical to find in
spreadsheets) we then needed to convert the CSV data into RDF triples. This
proved to be non-trivial for a number of reasons, many of which were not
forseen in the last report:

1. Representing statistical data in RDF is several orders of magnitude more
   complex than representing the data in tables.
2. The US severely lags behind other countries at publishing linked data
   vocabularies, example data sets and resources. As such, there is hardly any
   linked data available that would make sense to link our data to!
3. A side effect of (2) is that there are no official ontologies of other
   statistical data that we could use as a guideline to model our data.

Unfortunately having written the last report it was too late to change our data
set to something that would actually fit with the large web of linked data
available online.

Despite this, the W3's Data Cube Vocabulary [1] documents a "gold standard" for
publishing linked statistical data, and we have tried to follow their example
where possible.

Python's standard library CSV module was used to parse the CSV data:

~~~~{.python}
import csv

def get_bg_checks_total():
    with open('data-cleaned/bg_checks_total.csv', 'rb') as csvfile:
~~~~

And the widely recommended `rdflib` module was used to produce an in-memory
graph out of this CSV data. We set up rdflib with namespace and prefix bindings
for the vocabulary discussed below.

~~~~{.python}
import rdflib

def parse_bg_checks_total():
    g = rdflib.Graph()

    # Data Cube vocabulary
    qb = rdflib.Namespace("http://purl.org/linked-data/cube#")
    g.bind("qb", qb)

    # Dublin Core, some document publishing terms
    dc = rdflib.Namespace("http://purl.org/dc/terms/")
    g.bind("dc", dc)
~~~~

We then created a Data Structure Definition for the data. This will be
explained further in the "3rd party schemas" section.

The resulting graph object was then serialized into Turtle format.

~~~~{.python}
turtle = g.serialize(format="turtle")
with open("gun_stats.ttl", "w") as f:
    f.write(turtle)
~~~~

The file produced was then loaded into Jena to check that it was well-formed and
valid.

Before we could undergo any conversion process we needed to understand the
existing standards for representing statistical data as RDF. This is an active
area of research and discussion, and the most promising emerging standard
appears to be the W3's RDF Data Cube Vocabulary [http://www.w3.org/TR/vocab-data-cube/].

The Data Cube vocabulary is attractive as it is extremely flexible and provides
a clear conversion process from tabular to RDF data. Data is recorded as
"observations" which are linked to associated dimensions which can be user
defined or based on an accepted vocabulary, such as a time slice.

First, we add a data set object to the graph. This acts as a container for our
data set.

~~~~{.python}
# We are creating a data set
dataset = rdflib.URIRef(
    "http://vocab.inf.ed.ac.uk/dataset/fbi/background-checks")

# The dataset is of type a qb dataset
g.add((dataset, rdflib.RDF.type, qb['dataset']))

# The dataset has an rdfs comment explaining what the dataset is
g.add((dataset, rdflib.RDFS.comment,
    rdflib.Literal(
        "Total NCIS Federal Background Checks from 1998 - 2012",
        lang="en"
    )
))
~~~~

We then add a Data Structure Definition (DSD). This acts as a container which
contains the definition of measurements which we used in our data set.

~~~~{.python}
structure = rdflib.URIRef("http://vocab.inf.ed.ac.uk/dsd/fbi/background-checks")
g.add((dataset, qb['structure'], structure))

g.add((structure, rdflib.RDF.type, qb['DataStructureDefinition']))
~~~~

In the "Background Checks" example, we have two "axes" - a time period (a year
or a month), and the number of background checks performed. We represent those
in our data set as "components". For example, our axis of time is represented as
a "reference period".

~~~~{.python}
time_component = rdflib.BNode()
g.add((
    time_component,
    qb['measure'],
    sdmx_dimension['refPeriod']
))
g.add((
    time_component,
    qb['order'],
    rdflib.Literal(2)
))
~~~~

Our axis of "background checks" is simply a "measure" over this time period.

~~~~{.python}
background_checks_component = rdflib.BNode()
background_check = rdflib.URIRef('http://vocab.inf.ed.ac.uk/def/fbi/measure/background-check')

g.add((
    background_checks_component,
    qb['measure'],
    background_check
))

g.add((
    background_checks_component,
    qb['order'],
    rdflib.Literal(1)
))
~~~~

The "order" may not be relevant in this case as this is mainly used to guide
presentations as in [1].

We now add these components to our data structure, along with a "slice key"
which tell us that these components exist in an independent subset in our
data set, i.e. - that we only measure time and background checks together. This
slice key is given a URI Ref in case we need to use it again (we won't).

~~~~{.python}
g.add((structure, qb['component'], background_checks_component))
g.add((structure, qb['component'], time_component))
g.add((structure, qb['sliceKey'], rdflib.URIRef(
    'http://vocab.inf.ed.ac.uk/slice/fbi/background-checks-period'
)))
~~~~

Finally, having declared the structure of the data, we are now able to add
each observation to the graph as an object. Unfortunately we did not have the
time to look at the huge myriad of standards that exist for referencing time
periods, so they are simply added as literals in the format "1999" or "1999-2".

~~~~{.python}
for row in csvread.get_bg_checks_total():
    # Now we add our "observations"
    observation = rdflib.BNode()

    g.add((observation, rdflib.RDF.type, qb['observation']))
    g.add((observation, qb['dataset'], dataset))
    g.add((observation, sdmx_dimension['refPeriod'], rdflib.Literal(row[0])))
    g.add((observation, background_check, rdflib.Literal(row[1])))
~~~~

Our first data set is a set of statistics on how many background checks the
federal government has performed over a series of months and years.

One of the hallmarks of a well formed RDF document is to have as many URI
references as possible - these are the "links" in our linked data.
Unfortunately, there is no linked data vocabulary for "FBI Background Checks",
so we have to invent a few terms.

Perhaps one of the only elements in our data set which is reusable is "US
States". Both DBPedia and `data.gov` have ontologies for the US states
amongst *many* others. Thankfully most of these ontologies appear to link to
each other using `owl:sameAs` so it should be possible to correctly associate
them.

# List of Vocabularies

## RDF - http://www.w3.org/1999/02/22-rdf-syntax-ns#
Provies the "type" verb, expressing that objects belong to a class.

## RDFS - http://www.w3.org/2000/01/rdf-schema#
Provides a human readable "label" to our instances, useful especially for the
Data Cube vocabulary which can be intimidating at first.

## Dublin Core - http://purl.org/dc/elements/1.1/
Allows you to add publisher data to our Data Set.

## RDF Data Cube - http://purl.org/linked-data/cube#
Used to map our statistical data into RDF. Provides the Data Set, Data
Structure Definition and Observation vocabularies.

## SDMX Dimensions - http://purl.org/linked-data/sdmx/2009/dimension#
Short for "Statistical Data and Metadata Exchange", SDMX is an adopted subset
of RDF Data Cube. It provides a massive vocabulary for common measurements and
objects, and we use it as "time slice" seems to be the only "axis" we have in
common.

# Output

Some sample output is available at
http://oliver.kingshott.com/bg_checks.ttl

# Conclusion

We have missed the point completely. The team selected "something about guns"
because they read about it in the Guardian and saw "data". We have created
a data island that is useful to no-one, not an ontology that can link to the
vast wealth of information available from the web's linked data sets.

I would strongly recommend that we discard this data for part 2 of the
assignment, and perform SPARQL queries against endpoints which actually have
data that makes sense.

# References

[1] The Data Cube Vocabulary. http://www.w3.org/TR/vocab-data-cube/, retrieved February 2013
[2] Publishing Statistical Data Google Group. https://groups.google.com/group/publishing-statistical-data, retrieved February 2013
[3] Statistical Data and Metadata exchange: http://sdmx.org/, retrieved
February 2013
