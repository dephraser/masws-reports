# Conversion
To convert the data from CSV format we will produce a system written in Python
using one of the RDF libraries available for Python (such as rdflib or SuRF).
The produced script will read in the CSV and map it to an RDF graph in-memory.
It will then serialise this graph in Turtle format.

Alternatively we could attempt to make use of an automated conversion tool such
as `csv2rdf4lod` but the data seems simple and clean enough that a script would
be easier.
