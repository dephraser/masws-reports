# Multiagent Semantic Web Systems Coursework Reports

## Students

* Kit Barnes 
* David Fraser
* James Hulme
* Oli Kingshott

## Reports

1. Data Prelimary Report
2. Data Conversion Report

## Building reports

The LaTeX is DICE compatable. Reports can be made using `make report.pdf`
